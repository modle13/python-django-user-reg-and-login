"""Setuptools configuration."""
import os

from setuptools import find_packages, setup

import versioneer

HERE = os.path.dirname(os.path.abspath(__file__))


def extract_requires():
    """Get requirements from requirements.in."""
    with open(os.path.join(HERE, 'app/requirements/requirements.in'), 'r') as reqs:
        return [line.split(' ')[0] for line in reqs if not line[0] in ('-', '#')]


setup(
    name="generic_user_site_django_POC",
    description="A generic site for demonstration user form validation",
    url="",
    license='',
    author="Matthew Odle",
    author_email="odle.matthew@gmail.com",
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    # find_packages() only finds modules inside dirs that contain __init__.py
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=extract_requires(),
)
