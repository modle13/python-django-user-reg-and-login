FROM python:3.10

# Set env vars
ENV APPFILES_DIR=/usr/src/app
ENV VIRTUALENV_PATH=venv

# Make app user dir
RUN mkdir $APPFILES_DIR
RUN mkdir $APPFILES_DIR/staticfiles

# Give app user ownership of app user dir
RUN chown -R 1001:1001 $APPFILES_DIR

# Don't run as root
USER 1001

# Set the working directory
WORKDIR $APPFILES_DIR

# Make a virtualenv to avoid installing as root or in system directories
RUN python -m venv $VIRTUALENV_PATH

# Copy files to target dir in container
ADD app/ .

# Upgrade and install pip, setuptools and gunicorn
RUN . $VIRTUALENV_PATH/bin/activate && pip install --upgrade --no-cache-dir pip setuptools gunicorn

# Install the django app
RUN . $VIRTUALENV_PATH/bin/activate && pip install -U --no-cache-dir -r requirements/requirements.txt

# Gunicorn Runs on Port 8000
EXPOSE 8000

# Start command is handled by docker-compose; uncomment this to start a server standalone
ENV PATH="$VIRTUALENV_PATH/bin:$PATH"
CMD gunicorn $APP_NAME.wsgi --name=generic_site --workers=2 --max-requests=5 --max-requests-jitter=30 --bind 0.0.0.0:5000 --timeout 300
