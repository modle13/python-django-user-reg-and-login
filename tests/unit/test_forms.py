"""Unit Tests for Generic Site Forms"""
import pytest
from unittest import TestCase
from unittest.mock import Mock, patch

from generic_site import forms as test_module


class FormsTestCase(TestCase):
    def setUp(self):
        pass

    def test_save_raises_error_on_save_when_commit_false(self):
        test_form = test_module.MyUserCreationForm()

        expected = "Can't create User without database save"

        with pytest.raises(NotImplementedError) as exception_info:
            result = test_form.save(commit=False)

        self.assertEqual(expected, str(exception_info.value))
