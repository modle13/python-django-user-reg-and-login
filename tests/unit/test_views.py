"""Unit Tests for Generic Site Views"""
from unittest import TestCase
from unittest.mock import Mock, patch

from generic_site import views as test_module
from generic_site.forms import MyUserCreationForm


class ObjectFromDict(object):
    def __init__(self, dictionary):
        self.__dict__.update(dictionary)


class ViewsTestCase(TestCase):
    def setUp(self):
        pass

    @patch('generic_site.views.loader')
    def test_index_returns_rendered_template(self, loader_mock):
        test_request = Mock()

        loader_mock.get_template = Mock(return_value=Mock())
        template = Mock()
        template.render = Mock()

        result = test_module.index(test_request)

        self.assertEqual(result.status_code, 200)

    def test_register_returns_default_form_for_non_post(self):
        test_request = Mock()
        test_request.META = {}
        test_request.method = 'GET'

        # to check for a bit of the expected text
        expected = 'Required. 150 characters or fewer. Letters'

        result = test_module.register(test_request)

        self.assertIn(expected, str(result.content))

    @patch('generic_site.views.logger.error')
    @patch('generic_site.views.render')
    @patch('generic_site.views.MyUserCreationForm')
    def test_register_returns_default_form_for_post(self, userform_mock, render_mock, logger_mock):
        test_request = Mock()
        test_request.META = {}
        test_request.method = 'POST'
        test_username = 'some_username'

        userform_mock.is_valid = Mock(return_value=True)
        save_return_value = ObjectFromDict({'username': test_username})
        userform_mock.save = Mock(return_value=save_return_value)

        result = test_module.register(test_request)

        render_mock.assert_called_once()
        logger_mock.assert_not_called()
