"""Custom application views."""

import logging

from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader
from generic_site.forms import MyUserCreationForm

logger = logging.getLogger(__name__)


def index(request):
    """Define the default view behavior."""
    template = loader.get_template('index.html')
    context = {}
    return HttpResponse(template.render(context, request))


def register(request):
    """Define the register view behavior."""
    registered = False
    username = None

    if request.method == 'POST':
        user_form = MyUserCreationForm(data=request.POST)

        if user_form.is_valid():
            user = user_form.save()
            username = user.username
            registered = True
        else:
            logger.error(user_form.errors)

    else:
        user_form = MyUserCreationForm()

    return render(
            request,
            'registration/registration_form.html',
            {'user_form': user_form, 'registered': registered, 'username': username}
        )
