"""Custom applicatin forms."""

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class MyUserCreationForm(UserCreationForm):
    """Override default UserCreationForm."""

    class Meta:
        """Define Meta details for overridden form."""

        model = User
        fields = ['username', 'email', 'password1', 'password2']

    def save(self, commit=True):
        """Override the default save method."""
        if not commit:
            raise NotImplementedError("Can't create User without database save")
        user = super(MyUserCreationForm, self).save(commit=True)
        return user
