#!/usr/bin/env bash

source venv/bin/activate

source envs/local.env

export PYTHONPATH=$PYTHONPATH:$PWD/app

pytest -s --cov=app/generic_site/ \
          --cov-report=term-missing --cov-report=xml:coverage.xml --junitxml=test-report.xml \
          --cov-branch --cov-fail-under=90
