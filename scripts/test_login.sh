curl -c cookie.txt http://localhost:8000/accounts/login/

CSRF_TOKEN=$(sed -n 's/.*csrftoken\s//p' cookie.txt)

# need both the cookie file and the csrfmiddlewaretoken form field
# -L will cause curl to follow redirects
curl -L --cookie cookie.txt http://localhost:8000/accounts/login/ -F 'username=testuser1' -F 'password=batteryhorse' -F "csrfmiddlewaretoken=$CSRF_TOKEN"
