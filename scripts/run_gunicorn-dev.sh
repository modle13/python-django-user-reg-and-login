#!/usr/bin/env bash

source venv/bin/activate
source envs/local.env

pushd app

gunicorn generic_site.wsgi --name=generic_site --workers=2 --max-requests=5 --bind=0.0.0.0:8000

popd
