#!/usr/bin/env bash

source venv/bin/activate
set -a
. .env
set +a

pushd app

rm -rf app/static
python manage.py migrate --noinput

popd
