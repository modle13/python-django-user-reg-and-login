#!/usr/bin/env bash

docker-stop-all.sh
docker-remove-volumes.sh
docker-remove-containers.sh
docker image prune -a
