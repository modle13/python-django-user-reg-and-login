#!/usr/bin/env bash

source venv/bin/activate

set -a
. .env
set +a

pushd app

python manage.py runserver

popd
