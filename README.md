# Generic Site

See `DESIGN_NOTES.md` for more details on design decisions.

# Local Site API Testing

## Prerequisites:

- docker and docker-compose
- Postman

## Start docker-compose with the default configuration

```
docker-compose up -d
```

### Rebuild only the app with changes

```
docker-compose up -d --force-recreate --no-deps --build web
```

Note: application and `docker-compose` env vars are stored in `.env`

## Testing with a browser

8000 is the default django runserver port.

Access the test site with: http://localhost:8000

## Testing with Postman

Load the Postman 2.1 json collection into Postman: `generic-django-user-api.postman_collection.json`

Note: rendered HTML responses can be viewed via: 'Body' > 'Preview' in the response dialog.

Requests:
- home: loads the home page
- register-get-csrf: used to retrieve the csrftoken needed for registration page submissions (see below notes on retrieving the token)
- register: registration (edit data in the 'Body' tab)
- login-get-csrf: used to retrieve the csrftoken needed for login page submissions (see below notes on retrieving the token)
- login: logs in with provided user (edit data in the 'Body' tab)
- logout: logs the currently logged-in user out of the site

### CSRF token retrieval instructions

Since the site uses CSRF protection, the login and register form submission operations require an extra step.

Example using the register page; the login page flow will be the similar

1. execute the `register-get-csrf` request
2. click 'Cookies'
3. copy the value from the `csrftoken` 'Value' field
4. open the `register` request
5. paste the token value into the `csrfmiddlewaretoken` form field of the 'Body' section
6. set the other user values as needed
7. execute the `register` request
8. view the result with 'Body' > 'Preview'
9. repeat above workflow using for `login-get-csrt` and `login`

# User Schema details

The schema is managed with Django ORM.

The User model used is the default model. Please see documentation for field characteristics.

[Django User model docs](https://docs.djangoproject.com/en/4.0/ref/contrib/auth/)

Fields

```
username
first_name
last_name
email
password
groups
user_permissions
is_staff
is_active
is_superuser
last_login
date_joined
```

A note on passwords: Django uses the PBKDF2 algorithm with a SHA256 hash, and is considered to be sufficient for most Django users. [Django password docs](https://docs.djangoproject.com/en/4.0/topics/auth/passwords/#how-django-stores-passwords)

# Testing with curl (not all routes are captured as shell scripts)

Assumes a user exists with parameters:

* username: testuser1
* password: batteryhorse

Log in with `scripts/test_login.sh`

Log out with `scripts/test_logout.sh`

# Alternate run method for local dev using runserver

## Prerequisites:
- docker and docker-compose
- python3.8

## Set up virtualenv and install requirements

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements/requirements-dev.txt
```

## Collect static files, set up application schema via migrate, and start the django server

```
# copy static files to static serve directory
scripts/collectstatic.sh

# migrate models to database
scripts/migrate.sh

# using runserver
scripts/run.sh

# alternate using gunicorn
#scripts/run_gunicorn.sh
```

# Packaging the application

This application can be packaged as a tarball or wheel.

This operation uses versioneer to determine the target version from git repo information.

If versioneer is not already present, it can be installed with:

```
versioneer install
```

## Set up the virtualenv with dev tools

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements/requirements-dev.txt
```

## Build a package

```
python setup.py bdist_wheel # or sdist for a tarball
```

The package will be built to the `dist/` directory.

# Interacting with the postgres datastore

The Postgres datastore container can be connected to interactively by using `postgresql-client`

Installing `postgresql-client` on debian-based linux

```
sudo apt-get update
sudo apt-get install postgresql-client
```

Connecting to a postgresql instance

```
psql -h [HOSTNAME] -p [PORT] -U [USERNAME] -W
```

Creating a database

```
CREATE DATABASE [DATABASENAME];
```

Once the database exists, the `-d` flag can be used in the connection command.

```
psql -h [HOSTNAME] -p [PORT] -U [USERNAME] -W -d [DATABASENAME]
```

For example, to connect to our postgresql container running with `docker-compose.yml`

```
psql -h localhost -p 5432 -U demo -W -d demo
```

Connect to our postgresql container running with `docker-compose.yml`

```
psql -h postgres -p 5432 -U demo -W -d demo
```

Note `reverse-i-search` (ctrl+r) works for finding previous commands in psql mode.

List tables

```
demo-# \dt
                  List of relations
 Schema |            Name            | Type  | Owner 
--------+----------------------------+-------+-------
 public | auth_group                 | table | demo
 public | auth_group_permissions     | table | demo
 public | auth_permission            | table | demo
 public | auth_user                  | table | demo
 public | auth_user_groups           | table | demo
 public | auth_user_user_permissions | table | demo
 public | django_admin_log           | table | demo
 public | django_content_type        | table | demo
 public | django_migrations          | table | demo
 public | django_session             | table | demo
(10 rows)
```

List users

```
demo=# select * from auth_user;
```

List usernames, email addresses, date joined, and last login

```
demo=# select email, username, date_joined, last_login from auth_user;
```
