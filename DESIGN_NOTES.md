# Design choices

I chose django for several reasons
- requires minimal custom code; the app is only: 2 custom views, 1 overridden default form, 4 custom templates, 3 custom url routes
- provides built-in models and forms for user registration
 - these automatically handle field type validation for email, possword, username
  - Django passwords are stored with the PBKDF2 algorithm with a SHA256 hash
- provides automatic CSRF protection
- uses jinja for page templating
- uses a high-quality ORM to handle data models
- provides default forms and templates for common operations that are highly customizable

I overrode the default registration form to include the email field. The default just includes username and password fields.

I normally would use a better css framework like bootstrap or material, but for this exercise I only used a few basic css classes and ids.

There is one edge case that I would address: new users can be registered while a user is currently logged in. Instead what should happen is the registration endpoint redirects to the account's profile page. This can be handled in the registration view by applying a check for `request.user.is_authenticated`, and using a reverse redirect to index if so.

I also think it's important to capture common operations as scripts to speed up development feedback, and have placed several in the `scripts` directory.

# The Dockerfile

- Set up app user dir
- Create venv
- Copy files
- Perform app install
- The services

# The docker-compose.yml

- Gather static files, perform migrations
- Connects local disk as volume for postgres data and staticfiles via volumes
- Nginx was used to serve the Django static files
